import { createRouter, createWebHashHistory } from 'vue-router'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'// nprogress样式文件
const routes = [
  {
    path: '/',
    redirect: '/home'
  },
  {
    path: '/login',
    name: 'Login',
    // 通过函数来导入组件的话，此路由是懒加载，好处是不需要一次渲染或加载所有组件，当访问对应的路由时，再动态加载
    // 可以来提高咱们页面 第一次打开的速度
    component: () => import('../views/Login.vue')
  },
  {
    path: '/home',
    name: 'Home',
    // 通过函数来导入组件的话，此路由是懒加载，好处是不需要一次渲染或加载所有组件，当访问对应的路由时，再动态加载
    // 可以来提高咱们页面 第一次打开的速度
    component: () => import('../views/Home.vue'),
    children: [
      { path: '', component: () => import('../components/Welcome.vue') },
      { path: 'users', component: () => import('../components/Users.vue') },
      { path: 'roles', component: () => import('../components/Roles.vue') },
      { path: 'rights', component: () => import('../components/Rights.vue') },
      { path: 'goods', component: () => import('../components/Goods.vue') },
      { path: 'params', component: () => import('../components/Params.vue') },
      { path: 'categories', component: () => import('../components/Categories.vue') },
      { path: 'orders', component: () => import('../components/Orders.vue') },
      { path: 'reports', component: () => import('../components/Reports.vue') }
    ]
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})
// 配置路由导航前置守卫
router.beforeEach((to, from, next) => {
  NProgress.start()
  // 如果访问登录页 就直接放行
  if (to.path === '/login') return next()
  // 如果本地有token 代表已登录 放行
  if (window.sessionStorage.token) return next()
  // 如果请求的既不是访问登录页 又没有登录过 就让它回到登录页
  next('/login')
})
router.afterEach((to) => {
  NProgress.done()
})
export default router
