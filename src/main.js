import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// 导入ant-design-vue的相关资源
import './assets/css/base.css'
import './assets/css/global.css'
import {
  Button,
  Form,
  Select,
  Input,
  Layout,
  Menu,
  Breadcrumb,
  Table,
  Switch,
  Pagination,
  ConfigProvider,
  Drawer,
  Row,
  Col,
  Tag,
  Modal,
  Tree,
  Tooltip
} from 'ant-design-vue'
import 'ant-design-vue/dist/antd.css'
// 导入axios
import axios from 'axios'
// 创建app实例
const app = createApp(App)
// 配置axios基准路径
axios.defaults.baseURL = 'http://106.52.84.103:8888/api/private/v1/'
// 配置响应拦截器，来过滤数据，直接返回需要的data
axios.interceptors.response.use(res => {
  // Do something before request is sent
  return res.data
})

axios.interceptors.request.use(config => {
  // Do something before request is sent
  config.headers.Authorization = window.sessionStorage.token
  return config
})
// 将axios注册到全局
app.config.globalProperties.$http = axios

app.use(store)
  .use(router)
  .use(Button)
  .use(Form)
  .use(Select)
  .use(Input)
  .use(Layout)
  .use(Menu)
  .use(Breadcrumb)
  .use(Table)
  .use(Switch)
  .use(Pagination)
  .use(ConfigProvider)
  .use(Drawer)
  .use(Row)
  .use(Col)
  .use(Tag)
  .use(Modal)
  .use(Tree)
  .use(Tooltip)
  .mount('#app')
